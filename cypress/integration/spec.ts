describe('Test dynamic bookLists builder feature ', () => {
  it('check the add booklist functions', () => {
    cy.visit('/book/create')
    cy.get('[data-cy=add-booklist]').click()

    cy.get('[data-cy=booklist-input]')
      .should('be.visible')
  })
})
