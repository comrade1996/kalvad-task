import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { BookActions } from '../book-actions';
import { BooksService } from '../books.data';
import { Book, BookLists } from '../models/Book.model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  booksLists!: BookLists;
  constructor(private booksDataService: BooksService) { }

  ngOnInit(): void {
    this.booksDataService.BookListsObs.subscribe(data => this.booksLists = { ...data })
  }
  removeBookList(bookListName: string) {
    this.booksLists.booklists = this.booksLists.booklists.filter(bookList => bookList.booklistName != bookListName);
    this.updateBookListsState();
  }
  removeFromBookList(booklistIndex: number, bookTitle: string) {
    this.booksLists.booklists[booklistIndex].books = this.booksLists.booklists[booklistIndex].books.filter(book => book.title != bookTitle);
    this.updateBookListsState();
  }

  updateBookListsState() {
    this.booksDataService.setBookListState(this.booksLists, BookActions.UPDATE);
  }

  drop(event: CdkDragDrop<Book[]>, bookListIndex: number) {

    moveItemInArray(this.booksLists.booklists[bookListIndex].books, event.previousIndex, event.currentIndex);

    this.booksDataService.setBookListState(this.booksLists, BookActions.UPDATE);

  }
}
