import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BookActions } from './book-actions';
import { BookLists } from './models/Book.model';

@Injectable({
  providedIn: 'root'
})

/**
TODO Add another Layer of abstraction to make data access layer more loosely coupled
**/
export class BooksService {
  bookDb: BookLists = {
    "booklists":
      [{
        "booklistName": "Uncle BOB Collection", "isDisabled": true,
        "books": [
          { "title": "Clean Code", "author": "Uncle Bob", "year": "200", isDisabled: true },
          { "title": "Clean Architecture", "author": "Uncle Bob", "year": "2016", isDisabled: true }]
      }]
  };
  private booksMockDB = new BehaviorSubject<BookLists>(this.bookDb);

  constructor() { }

  get BookListsObs(): Observable<BookLists> {
    return this.booksMockDB.asObservable();
  }
  // TODO Refactor method by divide it to thinner methods
  setBookListState(booklist: BookLists, bookActions: BookActions) {


    switch (bookActions) {
      case BookActions.CREATE:
        {
          this.bookDb.booklists = [...booklist.booklists, ...this.bookDb.booklists];
          this.booksMockDB.next(this.bookDb);
        }
        break;
      case BookActions.UPDATE:
        {
          this.bookDb = { ...booklist };
          this.booksMockDB.next(this.bookDb);
        }
        break;
      case BookActions.DELETE:
        {
          this.bookDb = { ...booklist };
          this.booksMockDB.next(this.bookDb);
        }
        break;

      default:
        {
          this.booksMockDB.next(booklist);
        }
        break;
    }
  }
}
