//TODO Extract each of them one separate  file in large projects

export interface BookLists {
    booklists: BookList[]
  }
  
  export interface BookList {
    booklistName: string
    isDisabled: boolean
    books: Book[]
  }
  
  export interface Book {
    title: string
    author: string
    year: string
    isDisabled: boolean

  }