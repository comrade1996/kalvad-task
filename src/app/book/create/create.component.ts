import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { BookActions } from '../book-actions';
import { BooksService } from '../books.data';
import { Book, BookLists } from '../models/Book.model';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  BookListForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private booksDataService: BooksService,
    private router: Router
  ) { }

  ngOnInit() {
    this.BookListForm = this.fb.group({
      booklists: this.fb.array([]),
    });
  }

  booklists(): FormArray {
    return this.BookListForm.get('booklists') as FormArray;
  }

  newBookList(): FormGroup {
    return this.fb.group({
      booklistName: '',
      isDisabled: true,
      books: this.fb.array([]),
    });
  }

  addBookList() {
    this.booklists().push(this.newBookList());
  }

  removeBookList(booklistIndex: number) {
    this.booklists().removeAt(booklistIndex);
  }

  booklistBooks(booklistIndex: number): FormArray {
    return this.booklists().at(booklistIndex).get('books') as FormArray;
  }

  newBook(): FormGroup {
    return this.fb.group({
      title: '',
      author: '',
      year: '',
      isDisabled: true,
    } as Book);
  }

  addToBookList(booklistIndex: number) {
    this.booklistBooks(booklistIndex).push(this.newBook());
  }

  removeFromBookList(booklistIndex: number, bookIndex: number) {
    this.booklistBooks(booklistIndex).removeAt(bookIndex);
  }

  drop(event: CdkDragDrop<Book[]>, bookListIndex: number) {
    moveItemInArray(
      this.booklistBooks(bookListIndex).controls,
      event.previousIndex,
      event.currentIndex
    );
  

  }
  onSubmit() {
   
    this.booksDataService.setBookListState(
      this.BookListForm.value as BookLists,
      BookActions.CREATE
    );
    this.router.navigate(['book/index']);
  }

  resetForm() {
    this.booklists().clear();
  }
}
