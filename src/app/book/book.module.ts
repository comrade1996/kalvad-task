import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookRoutingModule } from './book-routing.module';
import { CreateComponent } from './create/create.component';
import { IndexComponent } from './index/index.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgIconsModule } from '@ng-icons/core';
import { HeroTrashSolid, HeroPlus, HeroPaperAirplane, HeroBan, HeroPencilAlt, HeroMenu, HeroCheck } from '@ng-icons/heroicons';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    CreateComponent,
    IndexComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DragDropModule,
    NgIconsModule.withIcons({ HeroCheck, HeroMenu, HeroPencilAlt, HeroTrashSolid, HeroPlus, HeroPaperAirplane, HeroBan }),
    BookRoutingModule
  ]
})
export class BookModule { }
