# Kalvad ng task


dynamic quiz builder using form arrays with Angular 13  SCSS  and Cypress
 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the dist/ directory.

## e2e Test

Run `npm run cypress:open` for run e2e tests

if the development server is not running you must run it first

## TODOs
Enhance ui/ux (Usability): 
i. Refactor ui by flowing ux designer or using ui framework to make ui better. ii. Add helpers like alerts - toasters -loaders ..etc to enhance ux.
iii. add validation check to create bookklists to  reduce users mistake


Enhance testability: 
i. add more tests cases to cypress and add unit testing to increase test coverage percentage.

Enhance scalability: 
i. achieve more loosely coupling by refactor the customHttpClient class.


Refactor TODOS in codebase